function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `
        <div class='card shadow' style="margin-bottom:20px;">

                <img src="${pictureUrl}" class="card-img-top" alt="Conference Image">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                    <p class="card-text">${description}</p>
                    <div class="card-footer">${startDate}-${endDate}</div>
            </div>
        </div>
    `;
}
function formatDate(dateString) {
    const options = { year: 'numeric', month: 'numeric', day: 'numeric' };
    const formattedDate = new Date(dateString).toLocaleDateString(options);
    return formattedDate;
}

window.addEventListener('DOMContentLoaded', async () => {
const url = 'http://localhost:8000/api/conferences/';

try {
    const response = await fetch(url);

    if (!response.ok) {
    // Handle bad response
    } else {
    const data = await response.json();
    // for styling
    let currentColumn = 0;

    // Clear previous content in the column
    // column.innerHTML = '';
    // console.log('data:', data)


    for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;  // Access the name directly from the conference object
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = formatDate(details.conference.starts);
            const endDate = formatDate(details.conference.ends);
            const locationName = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, startDate, endDate,locationName);
            const column = document.querySelector(`#col-${currentColumn}`)
            column.innerHTML += html;
            currentColumn += 1;
            if (currentColumn > 2) {
                currentColumn = 0;
            }
            // console.log('details:', details);
            // console.log('conference', conference)
        }
    }
    }
} catch (error) {
    handleError('An unexpected error occured. Please try again')
}
});
function handleError(errorMessage) {
    const alert = document.createElement('div');
    alert.className = 'alert alert-danger';

}
// window.addEventListener('DOMContentLoaded', async () => {
// const url = 'http://localhost:8000/api/conferences/';

// try {
//     const response = await fetch(url);

//     if (!response.ok) {
//     // Figure out what to do when the response is bad
//     } else {
//     const data = await response.json();

//     const conference = data.conferences[0];
//     const nameTag = document.querySelector('.card-title');
//     nameTag.innerHTML = conference.name;

//     const detailUrl = `http://localhost:8000${conference.href}`;
//     const detailResponse = await fetch(detailUrl);

    // if (detailResponse.ok) {
    //     const details = await detailResponse.json();
    //     console.log('details:,', details)
    //     // 1. Get the description data out of the details object
    //     const description = details.conference.description;
    //     // 2. Use querySelector to select the HTML element for description
    //     const descriptionTag = document.querySelector('.card-text');
    //     // 3. Set the innerHTML property of the HTML element to the description
    //     descriptionTag.innerHTML = description;

    //     // imageTag
    //     const imageTag = document.querySelector('.card-img-top');
    //     imageTag.src = details.conference.location.picture_url;
    //     console.log('Image Element:', imageTag);
    //     console.log('details', details)
    //   }
    // }
