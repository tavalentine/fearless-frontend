import React, {useState, useEffect} from 'react';

function ConferenceForm() {
  const [locations, setLocations] = useState([])
  const [formData, setFormData] = useState({
    name: '',
    starts: '',
    ends: '',
    description: '',
    max_presentations: '',
    max_attendees: '',
    location: '',
  })

  const getData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = 'http://localhost:8000/api/conferences/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    // console.log("Value:", e.target.value);
    // console.log("Name:", e.target.name);
    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea onChange={handleFormChange} value={formData.description} id="description" rows="3" name="description" className="form-control"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.max_presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange}  value={formData.max_attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;




// import React, { useState, useEffect } from 'react';

// function ConferenceForm({ getConferences }) {
//   const [name, setName] = useState('');
//   const [starts, setStarts] = useState('');
//   const [ends, setEnds] = useState('');
//   const [description, setDescription] = useState('');
//   const [maxPresentations, setMaxPresentations] = useState('');
//   const [maxAttendees, setMaxAttendees] = useState('');
//   const [location, setLocation] = useState('');
//   const [locations, setLocations] = useState([]);

//   async function fetchLocations() {
//     const url = 'http://localhost:8000/api/locations/';

//     const response = await fetch(url);

//     if (response.ok) {
//       const data = await response.json();
//       setLocations(data.locations);
//     }
//   }

//   useEffect(() => {
//     fetchLocations();
//   }, [])

//   async function handleSubmit(event) {
//     event.preventDefault();
//     const data = {
//       name,
//       starts,
//       ends,
//       description,
//       location,
//       max_presentations: maxPresentations,
//       max_attendees: maxAttendees,
//     };

//     const locationUrl = 'http://localhost:8000/api/conferences/';
//     const fetchConfig = {
//       method: "post",
//       body: JSON.stringify(data),
//       headers: {
//         'Content-Type': 'application/json',
//       },
//     };
//     const response = await fetch(locationUrl, fetchConfig);
//     if (response.ok) {
//       const newConference = await response.json();
//       console.log(newConference);
//       setName('');
//       setStarts('');
//       setEnds('');
//       setDescription('');
//       setMaxPresentations('');
//       setMaxAttendees('');
//       setLocation('');
//       getConferences()
//     }
//   }

//   function handleChangeName(event) {
//     const { value } = event.target;
//     setName(value);
//   }

//   function handleChangeStarts(event) {
//     const { value } = event.target;
//     setStarts(value);
//   }

//   function handleChangeEnds(event) {
//     const { value } = event.target;
//     setEnds(value);
//   }

//   function handleChangeDescription(event) {
//     const { value } = event.target;
//     setDescription(value);
//   }

//   function handleChangeMaxPresentations(event) {
//     const { value } = event.target;
//     setMaxPresentations(value);
//   }

//   function handleChangeMaxAttendees(event) {
//     const { value } = event.target;
//     setMaxAttendees(value);
//   }

//   function handleChangeLocation(event) {
//     const { value } = event.target;
//     setLocation(value);
//   }

//   return (
//     <div className="row">
//       <div className="offset-3 col-6">
//         <div className="shadow p-4 mt-4">
//           <h1>Create a new conference</h1>
//           <form onSubmit={handleSubmit} id="create-conference-form">
//             <div className="form-floating mb-3">
//               <input value={name} onChange={handleChangeName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
//               <label htmlFor="name">Name</label>
//             </div>
//             <div className="form-floating mb-3">
//               <input value={starts} onChange={handleChangeStarts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
//               <label htmlFor="starts">Starts</label>
//             </div>
//             <div className="form-floating mb-3">
//               <input value={ends} onChange={handleChangeEnds} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
//               <label htmlFor="ends">Ends</label>
//             </div>
//             <div className="mb-3">
//               <label htmlFor="description">Description</label>
//               <textarea value={description} onChange={handleChangeDescription} className="form-control" id="description" rows="3" name="description"></textarea>
//             </div>
//             <div className="form-floating mb-3">
//               <input value={maxPresentations} onChange={handleChangeMaxPresentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
//               <label htmlFor="max_presentations">Maximum presentations</label>
//             </div>
//             <div className="form-floating mb-3">
//               <input value={maxAttendees} onChange={handleChangeMaxAttendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
//               <label htmlFor="max_attendees">Maximum attendees</label>
//             </div>
//             <div className="mb-3">
//               <select value={location} onChange={handleChangeLocation} required name="location" id="location" className="form-select">
//                 <option value="">Choose a location</option>
//                 {locations.map(location => {
//                   return (
//                     <option key={location.id} value={location.id}>{location.name}</option>
//                   )
//                 })}
//               </select>
//             </div>
//             <button className="btn btn-primary">Create</button>
//           </form>
//         </div>
//       </div>
//     </div>
//   );
// }

// export default ConferenceForm;


// import React, { useState, useEffect } from 'react'

// function ConferenceForm() {
//     // Similar state strcture as LocationForm
//     const [name, setName] = useState('');
//     const [starts, setStartDate] = useState('');
//     const [ends, setEndDate] = useState('');
//     const [description, setDescription] = useState('');
//     // const [created, setCreated] = useState('');
//     // const [updated, setUpdated] = useState('');
//     const [presentations, setMaxPresentations] = useState('');
//     const [attendees, setMaxAttendees] = useState('');
//     const [location, setLocation] = useState([]);
//     const [selectedLocation, setSelectedLocation] = useState('');

//     const handleSubmit = async (event) => {
//         event.preventDefault();

//         const data = {}
//             data.name = name;
//             data.starts = starts;
//             data.ends = ends;
//             data.description = description;
//             data.max_presentations = presentations;
//             data.max_attendees = attendees;
//             data.location = parseInt(selectedLocation);
//             console.log('data:', data);

//         const conferenceUrl = 'http://localhost:8000/api/conferences/';
//         const fetchConfig = {
//             method: "post",
//             body: JSON.stringify(data),
//             headers: {
//                 'Content-Type': 'application/json',
//             },
//         };

//         const response = await fetch(conferenceUrl, fetchConfig);
//         console.log('response', response)
//         if (response.ok) {

//             const newConference = await response.json();
//             console.log('newConference', newConference);

//             setName('');
//             setStartDate('');
//             setEndDate('');
//             setDescription('');
//             setMaxPresentations('');
//             setMaxAttendees('');
//         }
//     }

//     const fetchData = async () => {
//         const url = 'http://localhost:8000/api/locations/';

//         try {
//         const response = await fetch(url);
//         console.log('locations:', response)
//         if (response.ok) {
//             const data = await response.json();
//             console.log('API response data:', data);
//             setLocation(data.locations);
//         } else {
//             console.log('Failed to fetch locations', response.statusText);
//         }
//         } catch (error) {
//         console.error('An error occurred during the fetch:', error.message);
//         }
//     };


//   useEffect(() => {
//     fetchData();
//   }, []);

//     // Placeholder event handlers
//     const handleNameChange = (event) => {
//         setName(event.target.value);
//     };

//     const handleStartChange = (event) => {
//         setStartDate(event.target.value);
//     };

//     const handleEndsChange = (event) => {
//         setEndDate(event.target.value);
//     };

//     const handleDescriptionChange = (event) => {
//         setDescription(event.target.value);
//     };

//     const handlePresentationChange = (event) => {
//         setMaxPresentations(event.target.value);
//     };

//     const handleAttendeeChange = (event) => {
//         setMaxAttendees(event.target.value);
//     };

//     const handleLocationChange = (event) => {
//         const selectedValue = event.target.value;
//         console.log('Selected Location ID:', selectedValue);
//         setSelectedLocation(selectedValue);
//     };
//     return (
//         <form onSubmit={handleSubmit} id="create-location-form">
//         <input
//             onChange={handleNameChange}
//             placeholder="Name"
//             required
//             type="text"
//             name="name"
//             id="name"
//             className="form-control"
//             value={name}
//         />
//         <input
//             onChange={handleStartChange}
//             placeholder="Starts"
//             required
//             type="date"
//             name="starts"
//             id="starts"
//             className="form-control"
//             value={starts}
//         />
//         <input
//             onChange={handleEndsChange}
//             placeholder="Ends"
//             required
//             type="date"
//             name="ends"
//             id="ends"
//             className="form-control"
//             value={ends}
//         />
//         <input
//             onChange={handleDescriptionChange}
//             placeholder="description"
//             required
//             name="description"
//             id="description"
//             className="form-control"
//             value={description}
//         />
//         <input
//             onChange={handlePresentationChange}
//             required
//             placeholder="Maximum presentations"
//             type="text"
//             name="Maximum presentations"
//             id="Maximum presentations"
//             className="form-control"
//             value={presentations}
//         />
//         <input
//             onChange={handleAttendeeChange}
//             required
//             placeholder="Maximum attendees"
//             type="text"
//             name="Maximum attendees"
//             id="Maximum attendees"
//             className="form-control"
//             value={attendees}
//         />
//         <select
//             onChange={handleLocationChange}
//             required
//             name="location"
//             id="location"
//             className="form-select"
//             value={selectedLocation}
//             >
//             <option value="">Choose a location</option>
//             {location.map((place) => (
//                 <option key={place.name} value={place.id}>
//                     {place.name}
//                 </option>
//             ))}
//             </select>

//             <button type="submit">Create</button>
//             </form>
//             );
//         }

//     export default ConferenceForm;
